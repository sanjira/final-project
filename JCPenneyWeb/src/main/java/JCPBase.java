import pageobject.*;

import java.io.IOException;

public class JCPBase extends Base {
    //For Sanjira
    //public static String testDataFilePath = "C:\\Users\\tariq\\Desktop\\final-project\\JCPenneyWeb\\src\\test\\TestData\\test_data.xlsx";
    //For Amit
    public static String testDataFilePath = "C:\\Users\\iamam\\Desktop\\final-project\\JCPenneyWeb\\src\\test\\TestData\\test_dataAmit.xlsx";
    //Test Data
    public String userName = readFromExcel(testDataFilePath,"Account","B2");
    public String password = readFromExcel(testDataFilePath,"Account","C2");
    public String productName1 = readFromExcel(testDataFilePath,"ProSearch","B2");
    public String productName2 = readFromExcel(testDataFilePath,"ProSearch","B3");
    public String zipCode = readFromExcel(testDataFilePath, "Zipcode", "A2");

    public JCPBase() throws IOException { }

    public void jcpLogin() throws InterruptedException {
        clickByXpath(AccountPage.signInIconXpath);
        clickByXpath(AccountPage.signInLinkXpath);
        sendTestByXpath(AccountPage.emailFieldXPath,userName);
        sendTestByXpath(AccountPage.passwordFieldXpath,password);
        clickByXpath(AccountPage.signInButtonXpath);

        Thread.sleep(2000);
    }

    public void jcpLogOut() throws InterruptedException{
        clickByXpath(AccountPage.signInIconXpath);
        clickByXpath(AccountPage.signOutXpath);
    }
    public void productSearch1() throws InterruptedException{

        clickByXpath(PSearchPage.searchField);
        sendTestByXpath(PSearchPage.searchField,productName1);
        clickByXpath(PSearchPage.searchIcon);
        Thread.sleep(3000);
    }
    public void productSearch2() throws InterruptedException{

        clickByXpath(PSearchPage.searchField);
        sendTestByXpath(PSearchPage.searchField,productName2);
        clickByXpath(PSearchPage.searchIcon);
        Thread.sleep(3000);
    }
    public void storeLocation() throws InterruptedException{

        clickByXpath(FindAStore.findStoreLinkXPath);
        sendTestByXpath(FindAStore.searchByFieldXPath, zipCode);
        clickByXpath(FindAStore.searchButtonXPath);
        Thread.sleep(3000);
    }
    public void clearanceInfo() throws InterruptedException{

        clickByXpath(Clearance.clearanceLinkXpath);
        clickByXpath(Clearance.imageLinkXpath);
        Thread.sleep(3000);
    }
    public void couponInfo() throws InterruptedException{

        clickByXpath(Clearance.couponLinkXpath);
        Thread.sleep(3000);
    }
    public void myAccount() throws InterruptedException{

        clickByXpath(MyPage.myAccountLinkXPath);
        Thread.sleep(3000);
    }
    public void trackOrder() throws InterruptedException{

        clickByXpath(MyPage.trackOrderLinkXPath);
        Thread.sleep(3000);
    }
    public void myWallet() throws InterruptedException{

        clickByXpath(MyPage.myWalletLinkXPath);
        Thread.sleep(3000);
    }
    public void myLists() throws InterruptedException{

        clickByXpath(MyPage.myListsLinkXPath);
        Thread.sleep(3000);
    }

    public void addToCart() throws InterruptedException{
        clickByXpath(CartPage.itemLinkXpath);
        clickByXpath(CartPage.clickOnAddToCartXpath);
        clickByXpath(CartPage.selectSizeIcon);
        sendTestByXpath(CartPage.selectSizeIcon,"Small");
        //clickByXpath(CartPage.sizefieldXpath);
        clickByXpath(CartPage.selectColorXpath);
        clickByXpath(CartPage.addToCartXpath);
        //clickByXpath(CartPage.cartIconXpath);
        Thread.sleep(3000);
    }

    public void showCart()throws InterruptedException{
        clickByXpath(CartPage.cartIconXpath);
        Thread.sleep(3000);
    }

    public void removeFromCart()throws InterruptedException{
        clickByXpath(CartPage.cartIconXpath);
        clickByXpath(CartPage.removeButtonXpath);
        Thread.sleep(3000);
    }

    public void myAppointment()throws InterruptedException{
        clickByXpath(MyPage.myAppointmentLinkXPath);
        Thread.sleep(3000);
    }

    public void kidsStoreSearch()throws InterruptedException{
        clickByXpath(StoreServicesPage.kidsZoneXpath);
        Thread.sleep(3000);
    }

    public void opticalServicesView()throws InterruptedException{
//        clickByXpath(StoreServicesPage.allStoreServicecViewXpath);
//        Thread.sleep(2000);
        clickByXpath(StoreServicesPage.jcpOpticalStoreXpath);
        Thread.sleep(9000);
    }

    public void jcpCareerPage()throws InterruptedException{
//        clickByXpath(StoreServicesPage.allStoreServicecViewXpath);
//        Thread.sleep(2000);
        clickByXpath(StoreServicesPage.jcpCareersPageView);
        Thread.sleep(9000);
    }

    public void giftRegistry()throws InterruptedException{
        clickByXpath(MyPage.giftRegistryLinkXPath);
        Thread.sleep(3000);
    }

    public void jewelryDiscountPage()throws InterruptedException {
        clickByXpath(DiscountPage.jewelryLinkXpath);
        Thread.sleep(3000);
    }

    public void homeAppliencesDiscountPage()throws InterruptedException {
        clickByXpath(DiscountPage.homeLinkXpath);
        Thread.sleep(3000);
    }

    public void kidsWearDiscountPage()throws InterruptedException {
        clickByXpath(DiscountPage.kidsLinkXpath);
        Thread.sleep(3000);
    }

    public void mensWearDiscountPage()throws InterruptedException {
        clickByXpath(DiscountPage.menLinkXpath);
        Thread.sleep(3000);
    }

    public void shoesDiscountPage()throws InterruptedException {
        clickByXpath(DiscountPage.shoesLinkXpath);
        Thread.sleep(3000);
    }

    public void accessiblePageView()throws InterruptedException {
        clickByXpath(MyPage.accessibleLinkXpath);
        Thread.sleep(3000);
    }



}