package pageobject;

public class AccountPage {

    //Login
    public static String signInIconXpath = "//*[@id=\"toggleToolTip\"]/div/div/div[1]";
    public static String signInLinkXpath = "//*[@id=\"headerAccount\"]/div/div/ul/li[1]/button";
    public static String emailFieldXPath = "//*[@id=\"content\"]/main/div[2]/div/div[2]/div/div[1]/div/div[2]/form/div[2]/div/div[1]/span/div/input";
    public static String passwordFieldXpath = "//*[@id=\"content\"]/main/div[2]/div/div[2]/div/div[1]/div/div[2]/form/div[2]/div/div[2]/input";
    public static String signInButtonXpath = "//*[@id=\"content\"]/main/div[2]/div/div[2]/div/div[1]/div/div[2]/form/div[2]/div/div[5]/button";

    //Logout
    public static String signOutXpath = "//*[@id=\"headerAccount\"]/div/div/ul/li[9]/a";
}
