package pageobject;

public class CartPage {
    // Add to Cart
    public static String itemLinkXpath = "//*[@id=\"secondaryNav\"]/div/div/ul/li[3]/a";
    public static String clickOnAddToCartXpath = "//*[@id=\"gallery-ppr5007738245\"]/button/span";
    public static String selectSizeIcon = "//*[@id=\"atPanelContent\"]/div/section/div/section[1]/section/section/div[1]/div/div[2]/div[1]/select";
    public static String sizefieldXpath = "//*[@id=\"atPanelContent\"]/div/section/div/section[1]/section/section/div[1]/div/div[2]/div[1]/select";
    public static String selectColorXpath = "//*[@id=\"atPanelContent\"]/div/section/div/section[1]/section/section/div[3]/div/div[2]/div[1]/div/ul/li[1]/div/button/div/img";
    public static String addToCartXpath = "//*[@id=\"atPanelContent\"]/div/section/div/section[2]/div[2]/div/div/button/span";

//    public static String clickOnItemXpath = "//*[@id=\"content\"]/main/article/div[2]/div[10]/div[2]/section/div[1]/div[3]/div/ul/li[4]/div/div/div/div/div[1]/div/a/div/div/img";
//    public static String selectSizeIcon = "//*[@id=\"content\"]/main/article/section/section[3]/div[2]/section[1]/section/div[1]/div[2]/div[2]/div[1]/select";
//    public static String sizeXpath = "//*[@id=\"content\"]/main/article/section/section[3]/div[2]/section[1]/section/div[1]/div[2]/div[2]/div[1]/select";
//    public static String selectColorXpath = "//*[@id=\"content\"]/main/article/section/section[3]/div[2]/section[1]/section/div[3]/div[1]/div[2]/div[1]/div/ul/li[1]/div/button/div/img";
//    public static String addToCartXpath = "//*[@id=\"GlobalOptions-AddToCart\"]/button/div/p[2]";

    //Show Cart
    public static String cartIconXpath = "//*[@id=\"header\"]/div[2]/ul/li[4]/div[4]/a/div";

    //Remove From Cart
    public static String removeButtonXpath = "//*[@id=\"content\"]/main/article/section[1]/section/span/div/div[1]/div[4]/div[1]/div[1]/div/div/ul/li[1]/div/div/div[2]/div[2]/div[1]/div[1]/div[4]/div/div[2]/div/button";

}
