package pageobject;

public class Clearance {

    //Clearance
    public static String clearanceLinkXpath = "//*[@id=\"secondaryNav\"]/div/div/ul/li[5]/a";
    public static String imageLinkXpath = "//*[@id=\"content\"]/main/article/div[2]/div[3]/div/div[1]/div/map/area[2]";

    //Coupons
    public static String couponLinkXpath = "//*[@id=\"secondaryNav\"]/div/div/ul/li[6]/a";


}
