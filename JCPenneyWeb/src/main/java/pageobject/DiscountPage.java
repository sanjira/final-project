package pageobject;

public class DiscountPage {
    public static String jewelryLinkXpath= "//*[@id=\"content\"]/main/article/div[2]/div[4]/div/div[1]/div/map/area[7]";
    public static String homeLinkXpath = "//*[@id=\"content\"]/main/article/div[2]/div[4]/div/div[1]/div/map/area[8]";
    public static String kidsLinkXpath = "//*[@id=\"content\"]/main/article/div[2]/div[4]/div/div[1]/div/map/area[4]";
    public static String menLinkXpath = "//*[@id=\"content\"]/main/article/div[2]/div[4]/div/div[1]/div/map/area[2]";
    public static String shoesLinkXpath = "//*[@id=\"content\"]/main/article/div[2]/div[4]/div/div[1]/div/map/area[5]";

}
