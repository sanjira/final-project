package pageobject;

public class FindAStore {

    //Finding Store Location by its Zip Code
    public static String findStoreLinkXPath = "//*[@id=\"header\"]/div[3]/div/div/div[1]/button/div[2]/div[2]";
    public static String searchByFieldXPath = "//*[@id=\"content\"]/main/article/div/div/div/div/div[1]/section/div/div/div[3]/div/div[3]/div[1]/div[1]/div[2]/span/input";
    public static String searchButtonXPath = "//*[@id=\"content\"]/main/article/div/div/div/div/div[1]/section/div/div/div[3]/div/div[3]/div[1]/div[3]/button";

}


