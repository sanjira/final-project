package pageobject;

public class MyPage {

    //My Account Info Page

    public static String myAccountLinkXPath = "//*[@id=\"content\"]/main/article/div/div[1]/ul/li[1]/button/div/div[2]/div[1]/div";

    //My Order Info Page

    public static String trackOrderLinkXPath = "//*[@id=\"content\"]/main/article/div/div[1]/ul/li[2]/button/div/div[2]/div[1]/div";

    // My Wallet Info Page

    public static String myWalletLinkXPath = "//*[@id=\"content\"]/main/article/div/div[1]/ul/li[4]/button/div/div[2]/div[1]/div";

    //My Lists Info Page

    public static String myListsLinkXPath = "//*[@id=\"content\"]/main/article/div/div[1]/ul/li[5]/button/div/div[2]/div[1]/div";

    //My Appointments Info Page

    public static String myAppointmentLinkXPath = "//*[@id=\"content\"]/main/article/div/div[1]/ul/li[7]/button/div/div[2]/div[1]/div";

    //Gift Registry

    public static String giftRegistryLinkXPath = "//*[@id=\"content\"]/main/article/div/div[1]/ul/li[9]/button/div/div[2]/div[1]/div";

    //Accessible Page

    public static String accessibleLinkXpath = "//*[@id=\"header-accessible-view\"]";

}
