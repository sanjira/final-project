package pageobject;

public class StoreServicesPage {

    //public static String allStoreServicecViewXpath = "//*[@id=\"footer\"]/div[3]/div/article/section/div[3]/div/div/ul/li[8]/a";
    public static String jcpOpticalStoreXpath = "//*[@id=\"footer\"]/div[3]/div/article/section/div[3]/div/div/ul/li[4]/a";

    public static String kidsZoneXpath = "//*[@id=\"footer\"]/div[3]/div/article/section/div[3]/div/div/ul/li[7]/a";

    public static String jcpCareersPageView = "//*[@id=\"footer\"]/div[3]/div/article/section/div[5]/div/div/ul/li[1]/a";
}
