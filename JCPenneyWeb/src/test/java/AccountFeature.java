import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class AccountFeature extends JCPBase {

    public AccountFeature() throws IOException {
    }

    @Test(priority = 1, enabled = false)
    public void testForSignIn() throws Exception {

        jcpLogin();
        Assert.assertEquals(driver.getTitle(),"My Dashboard - JCPenney");
        //takeTheScreenshot("LogIn");
    }

    @Test(priority = 6, enabled = false)
    public void testForSignOut() throws Exception {
        jcpLogin();
        jcpLogOut();
        //Assert.assertEquals(driver.getTitle()," ");
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/sessiontimeout");
        takeTheScreenshot("LogOut");
    }

    @Test(priority = 2, enabled = false)
    public void myAccountPage() throws Exception{
        jcpLogin();
        myAccount();

        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/s/flower+vases?Ntt=flower+vases");
        takeTheScreenshot("MyAccount");
    }

    @Test(priority = 3, enabled = false)
    public void trackOrderPage() throws Exception{
        jcpLogin();
        trackOrder();

        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/s/flower+vases?Ntt=flower+vases");
        takeTheScreenshot("TrackOrder");
    }

    @Test(priority = 4, enabled = false)
    public void myWalletPage() throws Exception{
        jcpLogin();
        myWallet();

        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/s/flower+vases?Ntt=flower+vases");
        takeTheScreenshot("MyWallet");
    }

    @Test(priority = 5, enabled = false)
    public void myListsPage() throws Exception{
        jcpLogin();
        myLists();

        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/s/flower+vases?Ntt=flower+vases");
        takeTheScreenshot("MyLists");
    }

    @Test(priority = 15, enabled = false)
    public void myAppointmentPage() throws Exception {
        jcpLogin();
        myAppointment();
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/account/dashboard/appointments");
        takeTheScreenshot("MyAppointment");
    }

    @Test(priority = 19, enabled = false)
    public void giftRegistryPage() throws Exception {
        jcpLogin();
        giftRegistry();
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/giftregistry");
        takeTheScreenshot("GiftRegistry");
    }

    @Test(priority = 25, enabled = true)
    public void accessiblePage() throws Exception {
        accessiblePageView();
        Assert.assertEquals(driver.getCurrentUrl(),"https://assistive.jcpenney.com/");
        takeTheScreenshot("AccessiblePage");
    }
}

