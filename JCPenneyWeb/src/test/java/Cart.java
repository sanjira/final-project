import org.testng.annotations.Test;

import java.io.IOException;

public class Cart extends JCPBase{

    public Cart() throws IOException {
    }
    @Test(priority = 12, enabled = true)
    public void cartTest1() throws Exception{
        addToCart();
        takeTheScreenshot("AddToCart");
    }

    @Test(priority = 13, enabled = true)
    public void cartTest2() throws Exception{
        jcpLogin();
        showCart();
        takeTheScreenshot("ShowCart");
    }
    @Test(priority = 14, enabled = true)
    public void cartTest3() throws Exception{
        jcpLogin();
        removeFromCart();
        takeTheScreenshot("RemoveFromCart");
    }
}
