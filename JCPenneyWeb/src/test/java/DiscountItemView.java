import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class DiscountItemView extends JCPBase{

    public DiscountItemView() throws IOException {
    }

    @Test(priority = 20, enabled = false)
    public void menWearDiscountPageView() throws Exception {
        mensWearDiscountPage();
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/g/men/N-bwo3y?pageType=X2H2&cm_re=ZA-_-IM-_-1015-HP-MEN");
        takeTheScreenshot("DiscountForMen");
    }

    @Test(priority = 21, enabled = false)
    public void kidsWearDiscountPageView() throws Exception {
        kidsWearDiscountPage();
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/g/kids/N-bwo40?pageType=X2H2&cm_re=ZA-_-IM-_-1015-HP-KIDS");
        takeTheScreenshot("DiscountForKids");
    }

    @Test(priority = 22, enabled = false)
    public void shoesDiscountPageView() throws Exception {
        shoesDiscountPage();
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/g/shoes/N-bwo42?pageType=X2H2&cm_re=ZA-_-IM-_-1015-HP-SHOES");
        takeTheScreenshot("DiscountForShoes");
    }

    @Test(priority = 23, enabled = false)
    public void jewelryDiscountPageView() throws Exception {
        jewelryDiscountPage();
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/g/jewelry-and-watches/N-bwo44?pageType=X2H2&cm_re=ZA-_-IM-_-1015-HP-JEWELRY-HUB");
        takeTheScreenshot("DiscountForJewelry");
    }

    @Test(priority = 21, enabled = false)
    public void homeAppliencesDiscountPageView() throws Exception {
        homeAppliencesDiscountPage();
        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/g/home-store/N-bwo3v?pageType=X2H2&cm_re=ZA-_-IM-_-1015-HP-FOR-THE-HOME");
        takeTheScreenshot("DiscountForTheHome");
    }

}
