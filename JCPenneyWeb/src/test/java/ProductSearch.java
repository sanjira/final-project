import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class ProductSearch extends JCPBase {

    public ProductSearch() throws IOException {
    }

    @Test(priority = 7, enabled = false)
    public void testSearchProduct1() throws Exception {
        productSearch1();

//        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/g/nike-shoes/N-bwo42Z6o?redirectTerm=Nike+Shoe");
//        takeTheScreenshot("NikeShoes");
    }

    @Test(priority = 8, enabled = false)
    public void testSearch2() throws Exception {
        productSearch2();

        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/s/flower+vases?Ntt=flower+vases");
        takeTheScreenshot("FlowerVases");
    }

    @Test(priority = 9, enabled = false)
    public void storeLocatorByZipcode() throws Exception {
        //jcpLogin();
        storeLocation();

        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/s/flower+vases?Ntt=flower+vases");
        takeTheScreenshot("Stores");
    }

    @Test(priority = 10, enabled = false)
    public void clearanceCheck() throws Exception {
        clearanceInfo();

        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/s/flower+vases?Ntt=flower+vases");
        takeTheScreenshot("Clearance");
    }

    @Test(priority = 11, enabled = false)
    public void couponCheck() throws Exception {
        couponInfo();

        //Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/s/flower+vases?Ntt=flower+vases");
        takeTheScreenshot("Coupon");
    }

}




