import org.testng.Assert;
import org.testng.annotations.Test;

import java.io.IOException;

public class StoreServicesView extends JCPBase{

    public StoreServicesView() throws IOException {
    }

    @Test(priority = 16, enabled = false)
    public void toFindKidsZone() throws Exception{
        kidsStoreSearch();

        Assert.assertEquals(driver.getCurrentUrl(),"https://www.jcpenney.com/m/kids-club?pageId=pg40069600006");
        takeTheScreenshot("KidsZone");
    }

    @Test(priority = 17, enabled = false)
    public void toOpticalServices() throws Exception{
        opticalServicesView();
        //Assert.assertEquals(driver.getTitle(),"Glasses");
        takeTheScreenshot("OpticalServicesView");
    }

    @Test(priority = 18, enabled = false)
    public void jcpCareerPageView() throws Exception{
        jcpCareerPage();
        takeTheScreenshot("JCPCareersPageView");
    }
}
